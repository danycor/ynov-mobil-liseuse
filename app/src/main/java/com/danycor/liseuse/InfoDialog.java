package com.danycor.liseuse;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

public class InfoDialog extends DialogFragment {
    public int wordCount;
    public int pageNumber;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_page, null);
        ((TextView) v.findViewById(R.id.page_word_count_text)).setText("Nb word : " + wordCount);
        ((TextView) v.findViewById(R.id.page_number_text)).setText("N° page : " + pageNumber);
        builder.setView(v);
        return builder.create();
    }


}
