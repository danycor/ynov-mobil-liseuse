package com.danycor.liseuse;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PageFragmentAdapter extends FragmentStatePagerAdapter {
    private List<String> pagesContent;
    public List<PageFragment> fragements;
    public PageFragmentAdapter(FragmentManager fm, List<String> pagesContent)
    {
        super(fm);
        this.pagesContent = pagesContent;
        fragements = new ArrayList<>();
        for (String pageContent: pagesContent) {
            int position = pagesContent.indexOf(pageContent);
            fragements.add(new PageFragment(pagesContent.get(position), position));
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragements.get(position);
    }

    @Override
    public int getCount() {
        return pagesContent.size();
    }
}
