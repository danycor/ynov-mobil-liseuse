package com.danycor.liseuse;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageFragment extends Fragment {
    private static final String ARG_PAGE_TEXT = "pageText";

    private String pageText;
    private int pageNumber;

    public PageFragment() {
        // Required empty public constructor
    }
    public PageFragment(String text, int pageNumber) {
        this.pageText = text;
        this.pageNumber = pageNumber;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 pageContent.
     * @return A new instance of fragment page_fragement.
     */
    public static PageFragment newInstance(String param1) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE_TEXT, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pageText = getArguments().getString(ARG_PAGE_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_page_fragement, container, false);
        ((TextView)v.findViewById(R.id.fragment_page_content)).setText(pageText);
        v.findViewById(R.id.fragment_page_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoDialog id = new InfoDialog();
                id.wordCount = pageText.split(" ").length;
                id.pageNumber = pageNumber +1;
                id.show(getFragmentManager(), "infoDialog");
            }
        });
        return v;
    }
}
